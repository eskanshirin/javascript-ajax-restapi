"use strict";
var g = {};
U.ready(function() {
  g.menugrap = U.$("menu");
  // initializeWelcome();
  defaulty();
  g.elementss = [];
  g.form = U.$("tab1");
  g.tab2 = U.$("tab2Content");
  createForm();
  createDateform();
  createnofarticlesform();
  g.changeTabs = U.addHandler(g.menugrap,"click", changetab);
  g.button = U.$("button");
  g.dateid  = U.$("date");
  g.numberid  = U.$("number");
  g.langg = U.$("lang");
  g.saveButton = U.$("savebutton");
  U.addHandler(g.saveButton, "click", savy);
  U.addHandler(g.button, "click", search);
  g.tab1 = U.$("tab1Content");
  g.tab2 = U.$("tab2Content");
  /*print saved favorite articles */
  var event = new Date();
  var currentDatefav = event.toJSON().replace(/-/g, "/").slice(0, 10);
  var favorites = localStorage.getItem("savefavorits");
  var val = JSON.parse(favorites);
  /*load saved articles from local storage it will help the method for top article construction*/
  if(favorites && val.elem.length !== 0 && val.duedate >= currentDatefav){
    for(var i = 0; i < val.elem.length;i++){
      request2(val.elem[i].source, val.elem[i].extract, val.elem[i].title, "", "", "tab2Content");
        }}
      /*when unchecked the saved articles they will disapear from the dom
      since reader does not like it anymore*/  
      U.addHandler(g.tab2,"change", function(e){
        e = e || window.event;
        var target = e.target || e.srcElement;
        if(target.type  === "checkbox" ){
          if( !target.checked ){
            var save2 = target.parentNode;
            emptyElement(save2);
            save2.remove();
          }
        }
      })

})
/** retrieve data from local storage and if there is it will use them to create
form elements each time page loades
**/
function processstat(){
  var statedate = localStorage.getItem("date1");
  var statelang = localStorage.getItem("lang1");
  var statenumber = localStorage.getItem("articlenum1");
  var statevisit = localStorage.getItem("visitDate");
  var event = new Date();
  var currentDatee = event.toJSON().replace(/-/g, "/").slice(0, 10);
  if( statedate || statelang || statenumber && statevisit > currentDatee ){
    return true;
  }
  return false;
}
//if user press save button page will save the data entry for date and language
//and number of article user input before, for 1 day
function savy(){
  var event = new Date();
  event.setDate(event.getDate() + 1);
  var jsonDate = event.toJSON().replace(/-/g, "/").slice(0, 10);
  localStorage.setItem("visitDate", jsonDate)
  localStorage.setItem("date1", g.dateid.value);
  localStorage.setItem("lang1", g.langg.value);
  localStorage.setItem("articlenum1", g.numberid.value);
}

/* manage the information of tabs to show with the upper tab */
function defaulty(){
  var cont = U.$("container");
  cont.children[0].style.visiblity = "visible";
  cont.children[1].style.visibility = "hidden";
  cont.children[2].style.visibility = "hidden";
  cont.children[0].style.display = "";
  cont.children[1].style.display = "";
  cont.children[2].style.display = "";
}
function changetab(e){
  e = e || window.event;
  var target = e.target || e.srcElement;
  defaulty();
  var cont = U.$("container");
  if(target.id === "tab11"){
    cont.children[0].style.visibility = "visible";
    cont.children[1].style.display = "none";
    cont.children[2].style.display = "none";
  }
  if(target.id === "tab22"){
    cont.children[0].style.display = "none";
    cont.children[1].style.visibility = "visible";
    cont.children[2].style.display = "none";
  }
  if(target.id === "tab33"){
    cont.children[0].style.display = "none";
    cont.children[1].style.display = "none";
    cont.children[2].style.visibility = "visible";
  }
}
/*create forms on div tab1 for language */
function createForm(){
  var formdiv = document.createElement("div");
  formdiv.id = "form";
  var label = document.createElement("label");
  label.setAttribute("for", "lang");
  label.textContent = "choose language";
  formdiv.appendChild(label);

  var select = document.createElement("select");
  select.setAttribute("name", "lang");
  select.id = "lang";
  select.setAttribute("required", "");
  formdiv.appendChild(select);
  var firstchildren = g.form.firstElementChild;
  g.form.insertBefore(formdiv, firstchildren);

  var option1 =  document.createElement("option");
  if(processstat()){
    var lang =  localStorage.getItem("lang1");

    option1.setAttribute("value", lang);
    option1.textContent = lang;
    select.appendChild(option1);
  }else{
    option1.setAttribute("value", "en");
    option1.textContent = "en";
    select.appendChild(option1);
  }

  var option2 =  document.createElement("option");
  if(option1.textContent === "en"){
    option2.setAttribute("value", "fr");
    option2.textContent = "fr";
    select.appendChild(option2);

  }else{
    option2.setAttribute("value", "en");
    option2.textContent = "en";
    select.appendChild(option2);
  }
}
function createDateform(){
  var formdiv2 = U.$("form");
  var label = document.createElement("label");
  label.setAttribute("for", "date");
  label.textContent = "choose Date";
  formdiv2.appendChild(label);

  //create inputtype
  var input = document.createElement("input");
  input.setAttribute("type", "date");
  input.id = "date";
  if(processstat()){
    var selectedDate = localStorage.getItem("date1");
    input.setAttribute("placeholder", "mm/dd/yy");
    input.setAttribute("value", selectedDate );
    label.appendChild(input);
  }else{
    input.setAttribute("placeholder", "mm/dd/yy");
    input.setAttribute("value", "05/08/2018");
    label.appendChild(input);
  }
}
//create form numberof articles
function createnofarticlesform(){
  var formdiv2 = U.$("form");
  var label = document.createElement("label");
  label.setAttribute("for", "number");
  label.textContent = "choose numberofarticles";
  formdiv2.appendChild(label);

  //create inputtype
  var input = document.createElement("input");
  input.setAttribute("type", "number");
  input.name = "number";
  input.id = "number";
  if(processstat()){
    var number = localStorage.getItem("articlenum1");
    input.setAttribute("value", number);
  }else{
    input.setAttribute("value", "5");
  }
  input.setAttribute("min", "5");
  input.setAttribute("max", "20");
  input.setAttribute("step", "5");
  label.appendChild(input);
  // create submitbutton
  var button = document.createElement("input");
  button.setAttribute("type", "button");
  button.id = "button";
  button.setAttribute("value", "register");
  formdiv2.appendChild(button);
  //save button
  var button2 = document.createElement("input");
  button2.setAttribute("type", "button");
  button2.id = "savebutton";
  button2.setAttribute("value", "save");
  formdiv2.appendChild(button2);
  var button3 = document.createElement("input");
  //reset
  button3.setAttribute("type", "button");
  button3.id = "resetbutton";
  button3.setAttribute("value", "reset");
  formdiv2.appendChild(button3);
}
