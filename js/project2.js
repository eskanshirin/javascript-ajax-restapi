"use strict";
var g = {};
U.ready(function() {
  g.changeTabs = U.addHandler(g.menugrap,"click", changetab);
  g.button = U.$("button");
  g.dateid  = U.$("date");
  g.numberid  = U.$("number");
  g.langg = U.$("lang");
  U.addHandler(g.button,"click", search);
  g.tab1 = U.$("tab1Content");
  g.tab2 = U.$("tab2Content");
  g.error = U.$("errors");
  /* if top article checked add it to saved place and if unchecked remove
  article we already added to save place as you dont like to save it */
  U.addHandler(g.tab1,"change", function(e) {
    e = e || window.event;
    var target = e.target || e.srcElement;
    if(target.type  === "checkbox" ){
      if( target.checked ){
        saveintab2(e);
      }
      if(!target.checked ){
        var savee = target.id + "saved";
        var element = U.$(savee);
        if(element !== null) {
          emptyElement(element);
          element.remove();
        }
      }
    }
  })

})
//start cookie
/*my cooklie doesnot work properly */
function initializeWelcome() {

  var name = "numvisited";

  var cookie = document.cookie;
  if(cookie.length === 0 ){
  var county=1;
  var expiration = new Date();
  expiration.setDate(expiration.getDate() + 7);
  var str = encodeURIComponent(name) + "=" + encodeURIComponent(county);
  str += ";expires=" + expiration.toGMTString();

      document.cookie = str;
    }else{
  var len = name.length;
  // Split the cookie string into a list of name=value
  var cookies = document.cookie.split(";");

  for (var i = 0, count = cookies.length; i < count; i++) {

  var pair = cookies[i].slice(0, 1) === " " ? cookies[i].slice(1) : cookies[i];

  pair = decodeURIComponent(pair);

  if (pair.slice(0, len) === name) {

    county = pair.split("=")[1];

     var expiration = new Date();
     expiration.setDate(expiration.getDate() + 7);
      str = encodeURIComponent(name) + "=" + encodeURIComponent(Number(county)+1);
     str += ";expires=" + expiration.toGMTString();
}
         document.cookie = str;

  }}
  }
/* when user checks an article it will add to saved part as a favorit articles
and if user uncheck it from top articles it will disapear from favorite saved
 articles. */
function saveintab2(e){
  e = e || window.event;
    var target = e.target || e.srcElement;
  var d = document.createElement("div");
  d.id = target.id + "saved";
  var x = target.parentNode;
  var copy = x.cloneNode(true);
  d.appendChild(copy);
  g.tab2.appendChild(d);
/* get elements I need from the div I chosed as favorit article
because cloneNode doesnt show me actual element to retrive from that later
and make local storage for saved article*/
  var element = {
    source : target.parentNode.children[5].src,
    extract :target.parentNode.lastChild.textContent,
    title : target.id,
    viewss : target.parentNode.children[3].textContent,
  }
  g.elementss.push(element);
  var event = new Date();
  event.setDate(event.getDate() + 1);
  var favoritDate = event.toJSON().replace(/-/g, "/").slice(0,10);
  localStorage.setItem("savefavorits", JSON.stringify({
    elem : g.elementss,
    duedate : favoritDate,
  }))
}

/*when user press register it will come here to see if data is in the localStorage
then it will use that and it will never send a request to wikipedia, if not it will
make a request*/
function search() {
  if (!g.dateid.value || !g.numberid.value ) {
    U.setText(g.error,"fill the form please and put date after  2015-07-01 ")
    setTimeout(function(){
      U.setText(g.error, "")}, 4000);
    return;
  }
  g.baseurlfirst =
  "https://wikimedia.org/api/rest_v1/metrics/pageviews/top/" + g.langg.value +
   ".wikipedia.org/all-access/"
  + buildQuery (g.dateid.value );
/*check local storage for toparticles if there is it will use local storage
if not it will make a request*/
  var currentDate = new Date();
  var usefulDate = currentDate.toJSON().replace(/-/g, "/").slice(0, 10);
  var value = localStorage.getItem(g.baseurlfirst);
  if(value ){
    var savedurl = JSON.parse(value);
    if(savedurl.saveDate >= usefulDate ){
      return processResponse(value);
    }else{
      localStorage.removeItem("url");
      return requesturlwiki(g.baseurlfirst);
    }
  }else {
    localStorage.removeItem("url");
    return requesturlwiki(g.baseurlfirst);
  }
}

function  buildQuery(date){
  var event = new Date(date);
  event.setDate(event.getDate() - 1);
  var jsonDate = event.toJSON().replace(/-/g, "/").slice(0, 10);
  return jsonDate;
}
function createRequestObject() {
  var request;
  if (window.XMLHttpRequest) {
    request = new XMLHttpRequest();
  } else {
    request = new ActiveXObject("Microsoft.XMLHTTP");
  }
  return request;
}
/* send first request for top articles*/
function requesturlwiki(url) {
  var request = createRequestObject();
  request.open("GET", url, true);
  U.addHandler(request,"load", function(){
    if(request.status >= 200 || request.status<=304 ){

      processResponse(request.responseText, url);
    } else if (request.status >= 400) {
          U.setText(g.error, "we can not get this article from website")
    setTimeout(function(){
      U.setText(g.error, "")}, 4000);
    }

  })
  request.send(null);
}
/*send second request for images*/
function requesturlwiki2(url, a, b, c) {
  var request = createRequestObject();
  request.open("GET", url, true);
  U.addHandler(request,"load", function(){
    if(request.status >= 200 || request.status<=304){

      var event = new Date();
      event.setDate(event.getDate() + 1);
      var jsonDateImage = event.toJSON().replace(/-/g, "/").slice(0, 10);
      localStorage.setItem(url, JSON.stringify({
        response2:request.responseText,
        imgName:a,
        lang : g.langg,
        imgDate : jsonDateImage,
      }))
      /*proceess response for second request*/
      processResponse2(request.responseText, a, b, c);
    } else if (request.status >= 400) {
        U.setText(g.error, "sorry we can not get back your request")
      setTimeout(function(){
           U.setText(g.error, "")}, 4000);
    }

  })
  request.send(null);
}

function processResponse(requesttext, url){
  var success = false;
  try {
    var wikiData = JSON.parse(requesttext);
    if(wikiData.language ){
      success = true;
      var j = 0;
      for(var i = 0; j < g.numberid.value ; i++){
        if (wikiData.response[i].article === "Main_Page" ||
     wikiData.response[i].article === "Special:Search" ||
     viewData.articles[i].article === "Special:Search" ||
     viewData.articles[i].article === "Special:" ||
      viewData.articles[i].article === "Special:CreateAccount" ||
     viewData.articles[i].article === "Special:RecentChangesLinked" ||
     wikiData.response[i].article === "Wikipédia:Accueil_principal"
   || wikiData.response[i].article === "Sp?cial:Search" ){

        }else{
          var url2  = "https://"  + g.langg.value  + ".wikipedia.org/api/rest_v1/page/summary/" +
  wikiData.response[i].article + "?redirect=false";
          search2(url2,  wikiData.response[i].article,
            wikiData.response[i].views,  wikiData.response[i].rank);
          j++;
        }
      }
    }
    if (wikiData.items && wikiData.items[0]) {

      success = true;
      var viewData = wikiData.items[0];
      var  local=[];
      for( i = 0; i < 35; i++){
        var title = viewData.articles[i];
        var lang = viewData.project;
        var rank = viewData.articles[i].rank;
        local.push(title);
      }
      var event = new Date();
      event.setDate(event.getDate() + 1);
      var saveRequestDate = event.toJSON().replace(/-/g, "/").slice(0,10);
      localStorage.setItem(url, JSON.stringify({
        response: local,
        saveDate : saveRequestDate,
        language:viewData.project,
        project : g.langg.value+".wikipedia",
        articles:local,
      }))
      j = 0;
      for( i = 0; j < g.numberid.value ;i++){
        if (viewData.articles[i].article === "Main_Page" ||
         viewData.articles[i].article === "Special:Search" ||
         viewData.articles[i].article === "Special:" ||
         viewData.articles[i].article === "Special:RecentChangesLinked" ||
         viewData.articles[i].article === "Special:CreateAccount" ||
         viewData.articles[i].article === "Wikipédia:Accueil_principal"||
         viewData.articles[i].article === "Sp?cial:Search"){

        }else{
          url2  = "https://"+ g.langg.value + ".wikipedia.org/api/rest_v1/page/summary/" +
          viewData.articles[i].article + "?redirect=false";

          search2(url2,local[i].article,local[i].views,local[i].rank);
          j++;
        }
      }
    }
    if (!success) {
      U.setText(g.error,
        "it seems something was wrong in request ")
      setTimeout(function(){
        U.setText(g.error, "")}, 4000);
    }
  } catch (error) {
    setTimeout(function(){
      U.setText(g.error,
        "it is not possible to process the data sent by website ")
      U.setText(g.error, "")}, 4000);
  }
}
function search2(url2, a, b, c){
  var value2 = localStorage.getItem(url2);
  var event = new Date();
  var currentDateimg = event.toJSON().replace(/-/g, "/").slice(0, 10);
  if(value2){
    var valueimg = JSON.parse(value2);
    if(valueimg.imgDate >= currentDateimg ){
      return processResponse2(valueimg.response2, a, b, c);
    }else{
      localStorage.removeItem(url2);
      return requesturlwiki2(url2, a, b, c);
    }
  }
  localStorage.removeItem(url2);
  return requesturlwiki2(url2, a, b, c);
}

function processResponse2(requesttext, a, b, c){

  var success = false;
  try {
    var wikiData2 = JSON.parse(requesttext);
    if(wikiData2.thumbnail){
      success = true;
      return request2(wikiData2.thumbnail.source, wikiData2.extract, a, b, c);
    }else {
      return request2(a, wikiData2.extract, a, b, c);
    }
    if(!success ){
        U.setText(g.error,
      "it seems something was wrong in request ")
      setTimeout(function(){
           U.setText(g.error,
         " ")}, 4000);
      }
  } catch (error) {
    U.setText(g.error,
      "we cant proccess this data ")
    setTimeout(function(){
      U.setText(g.error, "")}, 4000);
  }
}
function emptyElement(element){
  var myNode = element;
  while(myNode.firstChild){
    myNode.removeChild(myNode.firstChild)
  }
}
function request2 (source, extract, title, view, rank, whichtab) {

  g.div3 = document.createElement("div");
  g.div3.setAttribute("class", "outcontainer");
  g.div2 = document.createElement("div");
  g.div2.setAttribute("id", title);
  var label = document.createElement("label");
  label.setAttribute("class", "container3");
  label.setAttribute("for", title);
  var input = document.createElement("input");
  input.setAttribute("type", "checkbox");
  input.id = title;
  input.name = title;
  var span = document.createElement("span");
  span.setAttribute("class", "checkmark");
  var imgg = document.createElement("img");
  imgg.setAttribute("src", source);
  imgg.setAttribute("alt", title);
  imgg.setAttribute("width", "200");
  imgg.setAttribute("height", "200");
  imgg.setAttribute("class", "image");
  var para = document.createElement("p");
  U.setText(para, "  Viewers:=====>" + view)   ;
  var linkTitle = document.createElement("a");
  linkTitle.text = title;
  linkTitle.href = "https://" + g.langg.value + ".wikipedia.org/wiki/" + title;
  var para2 = document.createElement("p");
  U.setText(para2, extract );
  g.div2.appendChild(linkTitle);
  g.div2.appendChild(label);
  g.div2.appendChild(para);
  g.div2.appendChild(input);
  g.div2.appendChild(span);
  g.div2.appendChild(imgg);
  g.div2.appendChild(para2);
  g.div3.appendChild(g.div2);
  if(whichtab === "tab2Content"){
    input.setAttribute("checked", true);
    g.tab2.appendChild(g.div3);
  }else{
    g.tab1.appendChild(g.div3);
  }
}
